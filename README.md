# unbound

This Ansible role compiles `unbound` (a validating, recursive,
caching DNS resolver) from source and configures it to work
as the upstream DNS provider for a Pi-Hole on the same host.

This role has been tested on a Ubuntu Jammy virtual machine and on
Ubuntu Jammy on Raspberry Pi 3B and 4B.



## Links and notes

+ https://nlnetlabs.nl/projects/unbound


### unbound config

+ https://unbound.docs.nlnetlabs.nl/en/latest/manpages/unbound.conf.html
+ https://unbound.docs.nlnetlabs.nl/en/latest/getting-started/configuration.html
+ https://www.nlnetlabs.nl/documentation/unbound/howto-setup
+ https://www.linuxfromscratch.org/blfs/view/systemd/server/unbound.html
+ https://github.com/saint-lascivious/unbound-config
+ https://github.com/saint-lascivious/unbound-config/blob/master/configs/remote-control.conf
+ https://github.com/trinib/AdGuard-WireGuard-Unbound-DNScrypt/wiki/Build-Unbound-from-source
+ https://www.linuxbabe.com/ubuntu/set-up-unbound-dns-resolver-on-ubuntu-20-04-server
+ https://abridge2devnull.com/posts/2016/03/unbound-dns-server-cache-control
+ https://reddit.com/r/pihole/comments/daq9zo/unbound_gives_me_an_error_when_trying_to_access
+ https://dnswatch.com/dns-docs/UNBOUND
+ https://github.com/NLnetLabs/unbound/issues/138
+ https://discourse.pi-hole.net/t/compiling-unbound-from-source-getting-unbound-to-work-with-dns-over-tls/19017/5
+ https://stackoverflow.com/questions/60686991/unbound-dns-killed-immediately-after-starting
+ https://discourse.pi-hole.net/t/unbound-will-not-start/37533/1


### Lack of RTC on RPi can cause "interesting" failure modes

+ https://rachelbythebay.com/w/2024/04/10/rtc


### Returns SERVFAIL for domains that should work (fixed)

I am not sure what is going on here. For example

```
Apr 12 10:53:37 unbound[242139:0] error: SERVFAIL <www.powerinfotoday.com. AAAA IN>: all servers for this domain failed, at zone powerinfotoday.com. no server to query no addresses for nameservers
Apr 12 10:58:46 unbound[242139:0] error: SERVFAIL <www.chemistry-blog.com. AAAA IN>: all servers for this domain failed, at zone chemistry-blog.com. no server to query no addresses for nameservers
```

Both of these domains exist, which is easily demonstrated by looking them up from
a computer on a different network.

So why does `unbound` seem to suffer from partial blindness?

+ https://discourse.pi-hole.net/t/pihole-unbound-reply-error-is-servfail/43467/2

Hm, something made a recent change to `/etc/systemd/resolved.conf` (luckily it left
a copy of the original file around):
```
taha@bilbeis:~
$ diff /etc/systemd/resolved.conf /etc/systemd/resolved.conf.orig
31c31
< DNSStubListener=no
---
> #DNSStubListener=yes
```

So I have currently have `DNSStubListener=no`, whereas the original was the
(what I assume is the default) value of `yes`.
Judging from the modification time, I am guessing the installation of Pi-Hole
was responsible for changing this line (indeed it is, as Pi-Hole uses port 53
and setting `DNSStubListener=no` is the recommended way to make `systemd-resolved`
give way, so to speaK).

```
taha@bilbeis:~
$ sudo resolvectl status
Global
       Protocols: -LLMNR -mDNS -DNSOverTLS DNSSEC=no/unsupported
resolv.conf mode: uplink

Link 2 (enp5s0)
Current Scopes: DNS
     Protocols: +DefaultRoute +LLMNR -mDNS -DNSOverTLS DNSSEC=no/unsupported
   DNS Servers: 192.168.1.103
    DNS Domain: fs1
```

+ https://wiki.archlinux.org/title/Systemd-resolved

> `DNSStubListener=no` will switch off binding to port 53.
> https://unix.stackexchange.com/questions/304050/how-to-avoid-conflicts-between-dnsmasq-and-systemd-resolved

Making a DNS query via `systemd-resolved` uses unbound, which can be easily proven
by `resolvectl query <domain>` while tailing the unbound log. As expected, the
failing domains above still fail:
```
$ sudo resolvectl query powerinfotoday.com
powerinfotoday.com: resolve call failed: Could not resolve 'powerinfotoday.com', server or network returned error SERVFAIL
$ sudo resolvectl query dn.se
dn.se: 34.117.105.189                          -- link: enp5s0

-- Information acquired via protocol DNS in 44.5ms.
-- Data is authenticated: no; Data was acquired via local or encrypted transport: no
-- Data from: network
```

+ https://discourse.pi-hole.net/t/unbound-on-pi-hole-causing-servfail/62813/3

```
$ ll -R /etc/resolv*
lrwxrwxrwx 1 root root   39 mar 19 03:11 /etc/resolv.conf -> ../run/systemd/resolve/stub-resolv.conf
-rw-r--r-- 1 root root  790 apr  8 22:06 /etc/resolv.conf.bak
-rw-r--r-- 1 root root  500 okt 22  2021 /etc/resolvconf.conf

/etc/resolvconf:
total 12K
drwxr-xr-x   3 root root 4.0K apr  7 04:07 .
drwxr-xr-x 109 root root 4.0K apr  9 07:28 ..
drwxr-xr-x   2 root root 4.0K apr  7 04:11 update.d

/etc/resolvconf/update.d:
total 16K
drwxr-xr-x 2 root root 4.0K apr  7 04:11 .
drwxr-xr-x 3 root root 4.0K apr  7 04:07 ..
-rwxr-xr-x 1 root root 2.3K feb 14 20:23 dnsmasq
-rw-r--r-- 1 root root  661 feb 27 22:53 unbound
```

Let's inspect the above files (I will remove comments, empty lines, and such).
```
$ cat /etc/resolv.conf
nameserver 192.168.1.103
search fs1

$ cat /etc/resolv.conf.bak
nameserver 192.168.1.103
search fs1

$ cat /etc/resolvconf.conf
resolv_conf=/etc/resolv.conf
#name_servers=127.0.0.1
# Mirror the Debian package defaults for the below resolvers
# so that resolvconf integrates seemlessly.
dnsmasq_resolv=/var/run/dnsmasq/resolv.conf
pdnsd_conf=/etc/pdnsd.conf
unbound_conf=/etc/unbound/unbound.conf.d/resolvconf_resolvers.conf

$ cat /etc/resolvconf/update.d/unbound
#!/bin/sh -e
PATH=/usr/sbin:/usr/bin:/sbin:/bin
if [ ! -x /usr/sbin/unbound ]; then
    exit 0
fi
if [ ! -f /etc/unbound/unbound_control.key ]; then
    exit 0
fi
if [ ! -x /lib/resolvconf/list-records ]; then
    exit 1
fi
RESOLVCONF_FILES="$(/lib/resolvconf/list-records)"
if [ -n "$RESOLVCONF_FILES" ]; then
    NS_IPS="$(sed -rne 's/^[[:space:]]*nameserver[[:space:]]+//p' $RESOLVCONF_FILES \
        | egrep -v '^(127\.|::1)' | sort -u)"
else
    NS_IPS=""
fi
if [ -n "$NS_IPS" ]; then
    FWD="$(echo $NS_IPS | tr '\n' ' ')"
    unbound-control forward $FWD 1>/dev/null 2>&1 || true
else
    unbound-control forward off 1>/dev/null 2>&1 || true
fi
```

It looks like the last line in `/etc/resolvconf.conf` is a residue from the
unbound apt installation, and so is the entire `/etc/resolvconf/update.d/unbound`
file I think.

At this point it might be safer to nuke the VM and start over without ever installing
unbound from apt package and see if these same lines/files are created.

Also, it feels to me like all those lines saying `nameserver 192.168.1.103`
should be saying `nameserver 127.0.0.1#5353` or similar. I assume there is some
way to set this on the machine in question? It feels unnecessary that the server
acting as the recursive DNS server on the LAN itself takes it information on who
is the DNS server from the DHCP server, instead of simply always referencing itself.
Or is that somehow dangerous?

+ https://discourse.pi-hole.net/t/pi-hole-unbound-constant-servfail/68504
+ https://discourse.pi-hole.net/t/unbound-getting-random-servfail-for-any-domain/65761/4
+ https://discourse.pi-hole.net/t/unbound-starts-failing-when-i-set-pi-hdhozacole-to-use-it/52684
+ https://discourse.pi-hole.net/t/problems-with-unbound-unbound-servfail/54823
+ https://discourse.pi-hole.net/t/unbound-keep-getting-servfail/54807

On a VM reprovisioned from scratch the `/etc/resolvconf/update.d/unbound` file does not exist.
But `/etc/resolvconf.conf` looks the same, demonstrating that it is in fact not a residue
from the unbound apt install.
Therefore we should edit it to comment out those last lines (which this role now does).



### Public DNS servers

+ https://public-dns.info/nameserver/se.html



### Enabled "remote control"

Before setting remote control to `control-enable: yes`,
`unbound-control` commands will not work.

I have found `unbound-control` useful to flush specific domains,
for example after a power outage caused a DDNS change which meant
our DNS server used the wrong (cached) IP address of my DDNS domain,
and I had no patience to wait for the cache to expire by itself:

```
taha@bilbeis:~
$ sudo unbound-control dump_cache > ~/unbound.cache
taha@bilbeis:~
$ grep "my.example.se" unbound.cache
my.example.se.	3567	IN	A	81.226.27.51
msg my.example.se. IN AAAA 32896 1 3567 5 0 2 0
msg my.example.se. IN A 32896 1 3567 0 1 0 0
my.example.se. IN A 0
taha@bilbeis:~
$ sudo unbound-control flush_zone my.example.se
ok removed 1 rrsets, 2 messages and 0 key entries
```
