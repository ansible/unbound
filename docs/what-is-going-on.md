# Compiling unbound from source


## All looks well, except the systemd job always fails on start

Just start unbound manually. What happens?

```
taha@bilbeis:~
$ sudo -u unbound unbound -vvv -d -p -c /usr/local/etc/unbound/unbound.conf
[1712686382] unbound[182139:0] notice: Start of unbound 1.19.3.
Apr 09 20:13:02 unbound[182139:0] debug: creating udp4 socket 127.0.0.1 5353
Apr 09 20:13:02 unbound[182139:0] debug: creating tcp4 socket 127.0.0.1 5353
Apr 09 20:13:02 unbound[182139:0] debug: creating tcp4 socket 127.0.0.1 8953
Apr 09 20:13:02 unbound[182139:0] debug: setup SSL certificates
Apr 09 20:13:02 unbound[182139:0] debug: chdir to /var/lib/unbound
Apr 09 20:13:02 unbound[182139:0] warning: unable to initgroups unbound: Operation not permitted
Apr 09 20:13:02 unbound[182139:0] debug: drop user privileges, run as unbound
Apr 09 20:13:02 unbound[182139:0] debug: switching log to /var/lib/unbound/logs/unbound.log
```

The warning about initgroups vanishes if `unbound` is started as root:
```
taha@bilbeis:~
$ sudo unbound -vvv -d -p -c /usr/local/etc/unbound/unbound.conf
[1712795947] unbound[193178:0] notice: Start of unbound 1.19.3.
Apr 11 02:39:07 unbound[193178:0] debug: creating udp4 socket 127.0.0.1 5353
Apr 11 02:39:07 unbound[193178:0] debug: creating tcp4 socket 127.0.0.1 5353
Apr 11 02:39:07 unbound[193178:0] debug: creating tcp4 socket 127.0.0.1 8953
Apr 11 02:39:07 unbound[193178:0] debug: setup SSL certificates
Apr 11 02:39:07 unbound[193178:0] debug: chdir to /var/lib/unbound
Apr 11 02:39:07 unbound[193178:0] debug: drop user privileges, run as unbound
Apr 11 02:39:07 unbound[193178:0] debug: switching log to /var/lib/unbound/logs/unbound.log
```

The process starts and keeps ticking in either case.
So the problem must be due to something in the systemd service file.



### Whittle down the systemd job to the bare minimum

```
taha@bilbeis:~
$ cat /etc/systemd/system/unbound.service
[Unit]
Description=Unbound DNS server
After=network.target
Before=nss-lookup.target
Wants=nss-lookup.target

[Service]
Type=simple
ExecStart=/usr/local/sbin/unbound -vvv -p -c /usr/local/etc/unbound/unbound.conf

[Install]
WantedBy=multi-user.target
```

Restart and observe:
```
taha@bilbeis:~
$ sudo systemctl daemon-reload
$ sudo systemctl start unbound.service
$ sudo systemctl status unbound.service
● unbound.service - Unbound DNS server
     Loaded: loaded (/etc/systemd/system/unbound.service; enabled; vendor preset: enabled)
     Active: active (running) since Thu 2024-04-11 02:50:59 CEST; 11s ago
   Main PID: 193433 (unbound)
      Tasks: 1 (limit: 2211)
     Memory: 4.2M
        CPU: 86ms
     CGroup: /system.slice/unbound.service
             └─193433 /usr/local/sbin/unbound -vvv -p -c /usr/local/etc/unbound/unbound.conf
```

Seems to be running OK.
I confirmed this by changing Pi-Hole upstream to unbound and observing the log
(I decreased verbosity to make it easier to follow).


Now let's try to find which part of the original service file caused it to stall.
For reference, this is how that looked:
```
[Unit]
Description=Unbound DNS server
Documentation=man:unbound(8)
After=network.target
Before=nss-lookup.target
Wants=nss-lookup.target

[Service]
Type=notify
Restart=on-failure
EnvironmentFile=-{{ unbound_path_config_with_prefix }}
ExecStart={{ unbound.prefix }}/sbin/unbound -p -c {{ unbound_path_config_with_prefix }}
ExecReload=+/bin/kill -HUP $MAINPID

[Install]
WantedBy=multi-user.target
```

The `[Unit]` section was not the problem, as expected.

Let's test the lines in the `[Service]` section one by one.
`Type=notify` instead of `Type=simple`?

Well, that seems to cause the problem. The service appears to start normally, then
unceremoniously dies after about a minute. This is consistent with the failure
to start I have observed.

So, let's stick with `Type=simple`.

Also, although I have not tested it, let's drop the `EnvironmentFile` line (I prefer
to use the CLI flag `-c` to refer to the config file).
